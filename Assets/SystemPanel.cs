﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SystemPanel : MonoBehaviour {

	public Text currentBlock;
	public Text gridPos;
	public Text chunkName;
	public Text chunkPos;
	public Text data;

	public Text waterInstances;

	// Use this for initialization
	void Start () {
	
	}

	public void CountWaterInstances()
	{

	}
	string blockType;
	string gPos;
	string cName;
	string cPos;
	string dataT;

	// Update is called once per frame
	void Update () {
		Ray r = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast(r, out hit))
		{
			ChunkLogic c = hit.collider.GetComponent<ChunkLogic>();
			if (c == null)
			{
				if (hit.collider.name == "Background")
				{
					int cX = (int)hit.point.x / ChunkLogic.chunkWidth;
					int cY = (int)hit.point.y / ChunkLogic.chunkHeight;
					Vector3 cP = new Vector3(cX * ChunkLogic.chunkWidth, cY * ChunkLogic.chunkHeight, 0);
					if (ChunksManager.instance.ChunkExists(cP))
					{
						c = ChunksManager.instance.Chunks[cP];
					}
				}
			}
			if (c != null)
			{
				Vector2 p = c.getArrayCoordinates(hit.point.x, hit.point.y);
				BaseBlock b = c.GetBlock((int)p.x, (int)p.y);
				if (b != null)
				{
					blockType = b.GetType().ToString();
					gPos = ((int)p.x).ToString() + ", " + ( (int)p.y).ToString();
					cName = hit.collider.name;
					cPos = ((int) ((hit.point.x / ChunkLogic.chunkWidth) * ChunkLogic.chunkWidth)).ToString();
					cPos += ", " + ((int) ((hit.point.y / ChunkLogic.chunkHeight) * ChunkLogic.chunkHeight)).ToString();
					dataT = "Data: " + b.data.ToString();		
				}
			}

		}
		else
		{
			blockType = "";
			gPos = "";
			cName = "";
			cPos = "";
		}

		currentBlock.text = "Current Block: " + blockType;
		gridPos.text = "Grid Pos: " + gPos;
		chunkName.text = "Name: " + cName;
		chunkPos.text = "Pos: " + cPos;
		waterInstances.text = "Water Instances: " + Water.instancesCount.ToString ();
		data.text = dataT;
	}
}
