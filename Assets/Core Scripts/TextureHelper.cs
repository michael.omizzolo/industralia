﻿using UnityEngine;
using System.Collections;

public static class TextureHelper {

	public static float tUnit = 0.142857f;//0.25f;

	public static Vector2 Grass = new Vector2(0, 4);
	public static Vector2 Stone = new Vector2(0, 5);
	public static Vector2 Iron = new Vector2(3, 5);
	public static Vector2 Water = new Vector2(0,1);
	public static Vector2 Dirt = new Vector2(0,3);
}
