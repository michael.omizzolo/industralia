﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BattleFox
{
	public struct Bounds
	{
		public int x;
		public int y;
		public int w;
		public int h;
	}
}

public static class Generators  {

	public static void Initialize()
	{
		// Register generators here

		BattleFox.Bounds normalBounds = new BattleFox.Bounds () { x = -20, y = -50, w = 40, h = 240 };

		RegisterGenerator (normalBounds, new DefaultGenerator ());

	}

	public static Dictionary<BattleFox.Bounds, IGenerator> RegisteredGenerators = new Dictionary<BattleFox.Bounds, IGenerator>();

	public static void RegisterGenerator(BattleFox.Bounds bounds, IGenerator gen)
	{
		if (!RegisteredGenerators.ContainsKey(bounds))
		{
			RegisteredGenerators.Add(bounds, gen);
		}
	}

	public static IGenerator GetGenerator(float x, float y)
	{
		foreach(KeyValuePair<BattleFox.Bounds, IGenerator> pair in RegisteredGenerators)
		{
			if (x > pair.Key.x 
			    && x < pair.Key.x + pair.Key.w
			    && y > pair.Key.y
			    && y < pair.Key.y + pair.Key.h)
			{
				return pair.Value;
			}
		}
		return new DefaultGenerator ();
	}
}
