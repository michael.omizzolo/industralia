﻿using UnityEngine;
using System.Collections;

public interface IGenerator {

	void GenerateChunk(ChunkLogic c);
	Material generatorMaterial { get; }
	float tUnit { get; }
}
