﻿using UnityEngine;
using System.Collections;

public class TestGenerator : IGenerator {

	public Material generatorMaterial { get { return MaterialsHelper.instance.testMaterial; } }
	public float tUnit { get { return 0.047619f; } }

	public void GenerateChunk(ChunkLogic c)
	{
		for (int x = (int)c.transform.position.x; x < c.transform.position.x + ChunkLogic.chunkWidth; x++)
		{
			for (int y = (int)c.transform.position.y; y < c.transform.position.y + ChunkLogic.chunkHeight; y++)
			{
				if (y < 10)
					c.AddBlock(c.localX(x), c.localY(y), (int)TileType.Grass);
				else if (y < -20 && y > -30)
					c.AddBlock(c.localX(x), c.localY(y), (int)TileType.Stone);
				else
					c.AddBlock(c.localX(x), c.localY(y), (int)TileType.Empty);

			}
		}
	}
}
