﻿using UnityEngine;
using System.Collections;

public struct Tile
{
	public byte type;
	public byte data;
}