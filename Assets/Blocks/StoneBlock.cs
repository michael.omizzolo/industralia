﻿using UnityEngine;
using System.Collections;

public class StoneBlock : BaseBlock {


	public StoneBlock()
	: base((byte)TileType.Stone, 0)	{ TextureUV = TextureHelper.Stone;	}

	public override void Update ()
	{
		if (player.currentChunk == thisChunk)
		{
			if ((int)player.gridPos.x == gridX && (int)player.gridPos.y == gridY + 1)
			{
				player.GetComponent<SpriteRenderer>().color = Color.red;
			}
		}
	}
}
