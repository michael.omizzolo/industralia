﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

static class WaterLevel
{
	public const byte Empty = 0;
	public const byte MostlyEmpty = 50;
	public const byte HalfFull = 122;
	public const byte MostlyFull = 200;
	public const byte Full = 255;
	public const byte WaterUnit = 10;

	public static bool Almost(float lValue, byte rValue) { return (Mathf.Abs(lValue - (float)rValue) < WaterUnit); }
	public static bool Almost(byte lValue, byte rValue) { return (Mathf.Abs (lValue - rValue) < WaterUnit); }
	public static void RoundRoof(ref byte value) 
	{
		if (Almost(value, Full))
			value = Full;
		else if (Almost (value, MostlyEmpty))
			value = MostlyEmpty;
		else if (Almost(value, HalfFull))
			value = HalfFull;
		else if (Almost(value, MostlyFull))
			value = MostlyFull;
		else if (Almost(value, Empty))
			value = Empty;
	}
	public static byte Decrease(byte amount)
	{
		if (amount == Full)
			return MostlyFull;
		else if (amount == MostlyFull)
			return HalfFull;
		else if (amount == HalfFull)
			return MostlyEmpty;
		else if (amount == MostlyEmpty)
			return Empty;
		else
			return Empty;
	}
	public static byte Increase(byte amount)
	{
		if (amount == Full)
			return Full;
		else if (amount == MostlyFull)
			return Full;
		else if (amount == HalfFull)
			return MostlyFull;
		else if (amount == MostlyEmpty)
			return HalfFull;
		else
			return MostlyEmpty;
	}
}

public class Water : BaseBlock {

	public static int instancesCount = 0;
	public Water source;

	public UnityEngine.UI.Text waterLevelText;
	public float evaporateTimer = 0;
	int lastX;
	int lastY;
	public bool isSource = false;
	Vector2 flow = Vector2.zero;
	public float waterFillAmount = 0;

	public Water() :
	base((byte)TileType.Water, 0) {
		data = WaterLevel.HalfFull; // old
		instancesCount++;
		TextureUV = TextureHelper.Water - Vector2.up;
		//waterLevelText = (GameObject.Instantiate (Resources.Load ("WaterLevelText")) as GameObject).GetComponentInChildren<UnityEngine.UI.Text> ();
		lastX = gridX;
		lastY = gridY;
		waterFillAmount = 255;
		//isSource = (Random.Range (0.0f, 1.0f) > 0.8f);
	}


	~Water()
	{
		instancesCount--;
	}
	float timer = 0;
	public override void Update ()
	{
	//	waterLevelText.transform.parent.position = thisChunk.transform.position + new Vector3 (gridX + 0.5f, gridY - 0.5f, 0);
	//	waterLevelText.text = "";//pwaterFillAmount.ToString ();
		timer += Time.deltaTime;
		evaporateTimer += Time.deltaTime;
		if (timer > 0.1f)
		{
			byte d = data;
			if (!checkBottom())
			{
				checkBlock (rightBlock);
				checkBlock (leftBlock);
				BaseBlock[] rightSide = rightSideWaterBlocks();
				BaseBlock[] leftSide = leftSideWaterBlocks();
				float waterAmount = 0;
				int waterCells = rightSide.Length + leftSide.Length+1 /* of course, include ourselves in the calculation */ ;
				if (waterCells > 1)
				{
					foreach(BaseBlock b in rightSide)
						waterAmount += (b as Water).waterFillAmount;
					foreach(BaseBlock b in leftSide)
						waterAmount += (b as Water).waterFillAmount;
					waterAmount += waterFillAmount;
					waterAmount = waterAmount / waterCells;
					waterFillAmount = waterAmount;
					foreach(BaseBlock b in rightSide)
					{
						(b as Water).waterFillAmount = waterAmount;
						if ( (b as Water).waterFillAmount >= 254)
							(b as Water).waterFillAmount = 255;
					}
					foreach(BaseBlock b in leftSide)
					{
						(b as Water).waterFillAmount = waterAmount;
						if ( (b as Water).waterFillAmount >= 254)
							(b as Water).waterFillAmount = 255;
					}
					//if (WaterLevel.Almost(data, WaterLevel.Full))
					//	data = WaterLevel.Full; // --- OBSOLETE
				}	
			}
		}
		if (evaporateTimer > 0.8f)
		{
			if (lastX == gridX && lastY == gridY)
			{
				if (waterFillAmount <= WaterLevel.WaterUnit)
				{
					waterFillAmount = 0;
				}
			}
			evaporateTimer = 0;
		}
		if (waterFillAmount >= 254)
			waterFillAmount = 255;
		if (waterFillAmount <= 0)
		{			
			thisChunk.RemoveBlock(gridX, gridY);
		}


		lastX = gridX;
		lastY = gridY;
	}

	void SplitEvenly(ref float lValue, ref float rValue)
	{
		if (lValue < rValue)
		{
			float temp = lValue;
			lValue = rValue;
			rValue = temp;
		}
		float delta = (lValue - rValue);
		float amount = (delta / 2);
		lValue -= amount;
		rValue += amount;
	}
	
	void SplitEvenly(ref int lValue, ref int rValue)
	{
		if (lValue < rValue)
		{
			int temp = lValue;
			lValue = rValue;
			rValue = temp;
		}

		int delta = (lValue - rValue);
		int amount = (delta / 2);

		lValue -= amount;
		rValue += amount;

	}

	public override void OnBreak ()
	{
		
	}

	public BaseBlock[] leftSideWaterBlocks()
	{
		List<BaseBlock> rValue = new List<BaseBlock> ();
		BaseBlock current = leftBlock;
		while(current.type == (byte)TileType.Water)
		{
			rValue.Add(current);
			current = current.leftBlock;
		}
		return rValue.ToArray ();
	}

	public BaseBlock[] rightSideWaterBlocks()
	{
		List<BaseBlock> rValue = new List<BaseBlock> ();
		BaseBlock current = rightBlock;
		while(current.type == (byte)TileType.Water)
		{
			rValue.Add(current);
			current = current.rightBlock;
		}
		return rValue.ToArray ();
	}

	void checkBlock(BaseBlock b)
	{
		if (waterFillAmount <= WaterLevel.WaterUnit)
			return;
		if (b.type == (byte)TileType.Empty)
		{
			b = b.thisChunk.AddBlock(b.gridX, b.gridY, (byte)TileType.Water);

			(b as Water).waterFillAmount = 0;
			float d1 = waterFillAmount;
			float d2 = (b as Water).waterFillAmount;
			SplitEvenly(ref d1,ref d2);
			(b as Water).waterFillAmount = d2;
			waterFillAmount = d1;
			if (b is Water)
			{
				(b as Water).source = this;
				Vector2 _flow = new Vector2(b.gridX-gridX, b.gridY-gridY);
				(b as Water).flow = _flow;
			}
			thisChunk.GetComponent<ChunkDrawing>().UpdateMesh = true;
			b.thisChunk.GetComponent<ChunkDrawing>().UpdateMesh = true;
		}	
	}
	bool checkBottom()
	{
		Falling = false;
		BaseBlock b = bottomBlock;
		if (b == null)
		{
			Debug.Log("Hey! There's no block underneath? Go figure...");
			Debug.Log("BTW, the coords were " + bottomBlock.gridX + ", " + bottomBlock.gridY);
		}
		if (b.type == (byte)TileType.Empty)
		{
			Falling = true;
			//b.thisChunk.RemoveBlock(b.gridX, b.gridY);
			b = b.thisChunk.AddBlock(b.gridX, b.gridY, (byte)TileType.Water);
			(b as Water).waterFillAmount = waterFillAmount;
			waterFillAmount = 0;
			thisChunk.GetComponent<ChunkDrawing>().UpdateMesh = true;
			b.thisChunk.GetComponent<ChunkDrawing>().UpdateMesh = true;
			return true;
		}
		else if (b.type == this.type)
		{
			if ((b as Water).waterFillAmount < WaterLevel.Full)
			{			
				if ((b as Water).waterFillAmount + waterFillAmount > 255)
				{
					float remainder = 255 - (b as Water).waterFillAmount;
					(b as Water).waterFillAmount += remainder;
					waterFillAmount -= remainder;
				}
				else
				{
					(b as Water).waterFillAmount += waterFillAmount;
					waterFillAmount = 0;
				}
				Falling = true;
				thisChunk.GetComponent<ChunkDrawing>().UpdateMesh = true;
				b.thisChunk.GetComponent<ChunkDrawing>().UpdateMesh = true;
				return true;
			}
		}	
		return false;
	}

	public override void Render ()
	{

		float f = 1 - (waterFillAmount / 255);
		thisChunk.thisRenderer.AddFace (gridX, gridY - f,
		                                               gridX + 1, gridY - f,
		                                               gridX + 1, gridY - 1,
		                                               gridX, gridY - 1, this.TextureUV,(byte)waterFillAmount);
		                                               /*thisChunk.GetComponent<ChunkDrawing> ().AddWaterFace (gridX, gridY,
		                                                    gridX + 1, gridY,
		                                                    gridX + 1, gridY - 1,
		                                                    gridX, gridY - 1, this.TextureUV, this.waterFillAmount);*/
	}
}
