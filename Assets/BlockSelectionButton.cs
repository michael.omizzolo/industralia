﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class BlockSelectionButton : MonoBehaviour, IPointerClickHandler {
	public TileType Type;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	#region IPointerClickHandler implementation
	public void OnPointerClick (PointerEventData eventData)
	{
		BlockSelection.instance.SetCurrentBlock ((byte)Type);
	}
	#endregion
}
