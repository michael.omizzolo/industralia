﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(ChunkLogic))]
public class ChunkDrawing : MonoBehaviour {
	
	MeshFilter meshFilter;
	List<Vector3> verticesList = new List<Vector3>();
	List<int> trianglesList = new List<int>();
	List<Vector2> uvList = new List<Vector2>();
	List<Color32> colorList = new List<Color32>();
	public bool UpdateMesh = false;
	ChunkLogic c;
	// Use this for initialization
	void Start () {
		meshFilter = GetComponent<MeshFilter> ();
		StartCoroutine (delayedGeneration ());
		c = GetComponent<ChunkLogic>();
	}

	IEnumerator delayedGeneration()
	{
		yield return new WaitForEndOfFrame ();
		GenerateMesh ();
	}

	// Update is called once per frame
	void Update () {
		if (UpdateMesh)
		{
			GenerateMesh();
			UpdateMesh = false;
		}
	}

	void UpdateCollider()
	{
		GetComponent<MeshCollider> ().sharedMesh = null;
		GetComponent<MeshCollider> ().sharedMesh = meshFilter.mesh;
	}

	IEnumerator threadedMeshRender()
	{
		Mesh m = meshFilter.mesh;
		trianglesList.Clear ();
		uvList.Clear ();
		verticesList.Clear ();
		colorList.Clear ();

		System.Threading.Thread renderThread = new System.Threading.Thread(fillData);
		renderThread.Start();
		while(renderThread.IsAlive)
			yield return new WaitForEndOfFrame();

		m.Clear ();
		
		m.vertices = verticesList.ToArray ();
		m.triangles = trianglesList.ToArray ();
		m.uv = uvList.ToArray ();
		m.colors32 = colorList.ToArray ();
		m.Optimize ();
		
		m.RecalculateNormals ();
		
		GetComponent<MeshCollider> ().sharedMesh = null;
		GetComponent<MeshCollider> ().sharedMesh = meshFilter.sharedMesh;
		UpdateCollider ();

	}
	void fillData()
	{
		for(int y = 0; y < c.chunkBlocks.GetLength(1); y++)
		{
			for(int x = 0; x < c.chunkBlocks.GetLength(0); x++)
			{
				if (c.chunkBlocks[x,y].type != (byte)TileType.Empty)
				{
					c.GetBlock(x,y).Render();
				}
			}
		}
	}

	public void GenerateMesh()
	{
		StartCoroutine(threadedMeshRender());
	}

	bool validCoords(int x, int y)
	{
		ChunkLogic c = GetComponent<ChunkLogic> ();
		return (x >= 0 && y >= 0 && x < c.chunkBlocks.GetLength(0) && y < c.chunkBlocks.GetLength(1)
				&& x + 1 < c.chunkBlocks.GetLength (0)
				&& y - 1 >= 0);
	}

	public void AddWaterFace(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, Vector2 texture, float filling)
	{
		float f = 1 - ((float)filling / 255);
		
/*		if (WaterLevel.Almost(filling, WaterLevel.Full))
			f = 0;
		else if (WaterLevel.Almost(filling, WaterLevel.MostlyFull))
			f = 0.2f;
		else if (WaterLevel.Almost(filling, WaterLevel.HalfFull))
			f = 0.5f;
		else if (WaterLevel.Almost(filling, WaterLevel.MostlyEmpty))
			f = 0.7f;
		else if (WaterLevel.Almost(filling, WaterLevel.WaterUnit))
			f = 0.8f;
*/
		AddFace (x1, y1 - f, x2, y2 - f, x3, y3, x4, y4, texture, 18);
	}

	public void AddFace(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, Vector2 texture, byte alpha = 255)
	{
		int triangleIndexStart = verticesList.Count;
		verticesList.Add (new Vector3 (x1, y1, 0));
		verticesList.Add (new Vector3 (x2, y2, 0));
		verticesList.Add (new Vector3 (x3, y3, 0));
		verticesList.Add(new Vector3(x4, y4, 0));

		colorList.Add (new Color32 (255, 255, 255, alpha));
		colorList.Add (new Color32 (255, 255, 255, alpha));
		colorList.Add (new Color32 (255, 255, 255, alpha));
		colorList.Add (new Color32 (255, 255, 255, alpha));

		trianglesList.Add (triangleIndexStart);
		trianglesList.Add (triangleIndexStart + 1);
		trianglesList.Add (triangleIndexStart + 2);
		trianglesList.Add (triangleIndexStart);
		trianglesList.Add (triangleIndexStart + 2);
		trianglesList.Add (triangleIndexStart + 3);	
		float tUnit = 0.142f;//Generators.GetGenerator (transform.position.x, transform.position.y).tUnit;
		uvList.Add (new Vector2 (tUnit * texture.x, tUnit * texture.y + tUnit));
		uvList.Add (new Vector2 (tUnit * texture.x + tUnit, tUnit * texture.y + tUnit));
		uvList.Add (new Vector2 (tUnit * texture.x + tUnit, tUnit * texture.y));
		uvList.Add (new Vector2 (tUnit * texture.x, tUnit * texture.y));
	}

	

}
